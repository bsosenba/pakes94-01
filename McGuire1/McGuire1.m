% Pakes94.m (c) 2017 Benjamin S. Osenbach
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

%%%%%%%%%%%%%%%%%%%%%%%%% Pakes94 algorithm %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; clc;

maxN = 4;         		% Pakes-McGuire's original computational limit
a = 3;
wEntry = 4;
xEntryH = 0.25;
xEntryL = 0.15;
xCost = 1;
beta = 0.925;
delta = 0.7;
phi = 0.1;

wbar = 12;

Pi = CournotProfits(N); % change depending on market structure
X = zeros(N);
V = Pi;

diff = 1;
tol = 1e-6;
iter = 1;
maxiter = 1e16;

for N = 1:maxN

	while diff >= tol
    	X0 = X; V0 = V;
    	X = updateX(X);
    	V = updateV(V);
    	
    	Weight = (X-X0)'*(V-V0); % arbitrary stopping criterion
    	
    	diff = norm(Weight);	% need to code up the norm
    	
    	iter = iter + 1;

	    if iter = maxiter
    		disp('Maximum number of iterations exceeded');
    		break
    	end
    	
	end

	% Use the converged V and X from the N firm problem in the N+1 problem

end

%%%%%%%%%%%%%%%%%%%%%%%% Benjamin S Osenbach %%%%%%%%%%%%%%%%%%%%%%%%%%%%%