# Overview #

* This repository contains (simplified) MATLAB implementataions for the Pakes and McGuire (1994, RAND) algorithm (original code accessible at http://scholar.harvard.edu/pakes/pages/pakes-maguire-algorithm-0), the Pakes and McGuire (2001, Econometrica) stochastic algorithm, and benchmarking results for both algorithms

## Setup ##

* To run the code, simply copy the m-files into the working directory of your MATLAB / GNU Octave installation
* Scripts containing the main loop are named after their parent directories

## Contribution guidelines ##

* In the spirit of academic review, the MATLAB code contained in this repository is hereby released under the GNU General Public License v.3

## Questions ##

* Contact Benjamin S. Osenbach (bsosenba@gmail.com) with any questions pertaining to this repository