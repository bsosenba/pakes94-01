% checkEntry.m (c) 2017 Benjamin S. Osenbach
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [ enter, where ] = checkEntry( V, phi, z, N)
%UNTITLED8 Summary of this function goes here
%   Detailed explanation goes here

    if V(z, N) <= phi
        enter = 0;
    end


    ptau = a * x / (1+a * x);

    tau = rand(1,1) < ptau ; % simulate Bernouli trial for tau

    nu = rand(1,1) < delta ;

end

