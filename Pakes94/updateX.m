% updateX.m (c) 2017 Benjamin S. Osenbach
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [ Xnew ] = updateX( Xold )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here

    z = length( Xold(1,:) ); % get the number of rows
    N = length( Xold(:,1) ); % get the number of cols
    
    for j = 1:N              % each column represents a single firm
        for i = 1:z
            % do stuff to Xold
        end
    end
    
    Xnew = Xold;
    
end

