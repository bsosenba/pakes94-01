% Binom.m (c) 2017 Benjamin S. Osenbach
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.

function [ Binom ] = Binom( wbar )
%Binom Returns a matrix
%   Returns a matrix [0, B], where B is a lower-triangular matrix of
%   Pascal's Triangle

    Binom = zeros(wbar);
    for i = 1:wbar
        for j=1:i % lower-triangular matrix
            Binom(i,j) = nchoosek(i-1,j-1);
        end
    end
    
    Binom = [zeros(wbar, 1) , Binom];

end

